﻿$(document).ready(function() {
	jQuery(window).bind("load resize", resizeFrame);
	var toggled = false;
	function resizeFrame(){
		var backgroundRatio = $("#bg-picture").width() / $("#bg-picture").height();
		var contentHeight = window.innerHeight - $("#header").height() - $("#footer").height();
		var articleHeight = contentHeight - $("#sub-nav").outerHeight();
		var backgroundWidth = contentHeight*backgroundRatio;
		var contentRatio = window.innerWidth / contentHeight;

		$("#index-spacer").css("height", contentHeight + 'px');	//adjust content size
		$("#sub-nav-strip").css("width", $("#header").width() + 'px');
		$("#article").css("height", articleHeight + 'px');
		
		if(!toggled)	//normal mode
		{
			$("#bg-picture").css("position", 'fixed').css("left", '0px'); //reset centering and dots
			if( backgroundRatio < contentRatio )
			{	$("#bg-picture").css("width", '100%').css("height", 'auto');}
			else 
			{	$("#bg-picture").css("width", backgroundWidth + 'px').css("height", contentHeight + 'px');}
		}
		else	//content hidden
		{
			$("#bg-picture").css("width", 'auto').css("height", contentHeight + 'px');
			$("#bg-picture").css("position", 'static');
		}

		$("#header-bar, #content, #rightArrow, #toggleContent").toggle(); //flush content
		$("#header-bar, #content, #rightArrow, #toggleContent").toggle(); //remove traces

		$("#overlay").css("height", contentHeight + 'px');
		$("#background").css("height", contentHeight + 'px');
	}
	resizeFrame();
	
/*Toggling visibility of content*/	
	function hideContent() {
		toggled = true;		//hide content & reveal a photo & unmute video
		resizeFrame();
		$("video").prop('muted', false);
		var toggleButton = $("#toggleContent");
		var src = toggleButton.css("background-image");
		toggleButton.css("background-image", src.replace('hide_content.png', 'show_content.png'));
		toggleButton.attr('title', 'Текст');
		$("#sub-nav-strip, #content, #overlay, #leftArrow, #rightArrow").css("visibility", "hidden");
	};
	function showContent() {
		toggled = false;	//show content
		resizeFrame();
		$("video").prop('muted', true);
		jQuery(window).bind("load resize", resizeFrame);
		var toggleButton = $("#toggleContent");
		var src = toggleButton.css("background-image");
		toggleButton.css("background-image", src.replace('show_content.png', 'hide_content.png'));
		toggleButton.attr('title', 'Галерея');
		$("#sub-nav-strip, #content, #overlay, #leftArrow, #rightArrow").css("visibility", "visible");
	}
	$(".items img").click( function() {
		if(!toggled)
		{	$("#toggleContent").click();}}
	);
	$("#toggleContent").toggle(hideContent, showContent);
	
// Custom scroll bars	
	$('#article').each(
		function()
		{
			$(this).jScrollPane(
				{
					showArrows: $(this).is('.arrow')
				}
			);
			var api = $(this).data('jsp');
			var throttleTimeout;
			$(window).bind(
				'resize',
				function()
				{
					if ($.browser.msie) {
						// IE fires multiple resize events while you are dragging the browser window which
						// causes it to crash if you try to update the scrollpane on every one. So we need
						// to throttle it to fire a maximum of once every 50 milliseconds...
						if (!throttleTimeout) {
							throttleTimeout = setTimeout(
								function()
								{
									api.reinitialise();
									throttleTimeout = null;
								},
								50
							);
						}
					} else {
						api.reinitialise();
					}
				}
			);
		}
	);

});