'''
Created on Apr 9, 2012

@author: nailgun
'''
from django.contrib import admin
from annodomini.projects.models import Project, Category, Photo

class PhotoInline(admin.StackedInline):
    model = Photo

class ProjectAdmin(admin.ModelAdmin):
    prepopulated_fields = { 'slug': ['name'] }
    inlines = [PhotoInline]
    list_display = ('name', 'category', 'order',)
    
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = { 'slug': ['name'] }
    
class PhotoAdmin(admin.ModelAdmin):
    list_display = ('normal_view', 'project', 'order')
    
    def has_add_permission(self, request):
        return False

admin.site.register(Project, ProjectAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Category, CategoryAdmin)