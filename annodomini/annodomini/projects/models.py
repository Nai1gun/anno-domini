from django.db import models
from easy_thumbnails.files import get_thumbnailer
from django.db.models.fields.files import ImageField
from annodomini.util.fields import WYSIWYGField

class Category(models.Model):
    name = models.CharField(max_length=250)    
    slug = models.SlugField(unique=True)
    
    class Meta:
        verbose_name_plural = "Categories"
    
    def __unicode__(self):
        return self.name

class Project(models.Model):
    name = models.CharField(max_length=250)
    slug = models.SlugField(unique=True)
    thumbnail = ImageField(upload_to='projects/thumbnail')
    category = models.ForeignKey(Category, related_name='projects')
    preliminary_text = WYSIWYGField(blank=True, null=True)
    supporting_information  = WYSIWYGField(blank=True, null=True)
    main_text = WYSIWYGField(blank=True, null=True)
    order = models.PositiveIntegerField(blank=True, null=True)
    
    class Meta:
        ordering = ['category', 'order', 'name']
    
    def __unicode__(self):
        return self.name
            
class Photo(models.Model):
    image = models.ImageField(upload_to='projects/photo')
    comment = models.CharField(max_length=250, blank=True, null=True)
    project = models.ForeignKey(Project, related_name='photos')
    order = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ['project__name', 'order']
       
    def normal_view(self):
        """
        This is a helper thumbnail shown in admin
        """
        thumbnailer = get_thumbnailer(self.image, relative_name=self.image.name)
        thumb = thumbnailer.get_thumbnail({'size': (100, 0)})        
            
        return thumb.tag()
       
    normal_view.allow_tags = True
    
    def __unicode__(self):
        return str(self.image.url.split("/")[-1])