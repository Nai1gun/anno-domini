from django.views.generic.detail import DetailView
from annodomini.projects.models import Project, Category
from django.views.generic.base import RedirectView
from django.http import Http404
from annodomini.util.models import get_next, get_prev

class ProjectDetailView(DetailView):

    context_object_name = "project"
    model = Project

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ProjectDetailView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the categories
        context['category_list'] = Category.objects.all()
        # Add previous and next urls
        project = context['project']
        context['next'] = "/projects/%s/%s/" % (project.category.slug,
            get_next(Project.objects.filter(category=project.category), project, True).slug)
        context['prev'] = "/projects/%s/%s/" % (project.category.slug,
            get_prev(Project.objects.filter(category=project.category), project, True).slug)
        return context
    
class CategoryDetailView(DetailView):

    context_object_name = "category"
    model = Category

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(CategoryDetailView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the categories
        context['category_list'] = Category.objects.all()
        category = context['category']
        
        next = get_next(Category.objects.all(), category, False)
        if next == None:
            context['next'] = '/production/'
        else:            
            context['next'] = '/projects/%s/' % next.slug
            
        prev = get_prev(Category.objects.all(), category, False)
        if prev == None:
            context['prev'] = '/news/'
        else:            
            context['prev'] = '/projects/%s/' % prev.slug
            
        return context

class ProjectsRedirectView(RedirectView):
    url = "/projects/"

    def get_redirect_url(self, **kwargs):
        qs = Category.objects.all()
        l = list(qs[:1])
        if l:
            first = l[0]
            redirect_url = "/projects/%s/" % first.slug
            return redirect_url
        raise Http404