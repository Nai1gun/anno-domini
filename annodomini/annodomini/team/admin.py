'''
Created on Apr 8, 2012

@author: nailgun
'''
from django.contrib import admin
from annodomini.team.models import Person

class PersonAdmin(admin.ModelAdmin):
    prepopulated_fields = { 'slug': ['name'] }
    list_display = ('name', 'order',)

admin.site.register(Person, PersonAdmin)