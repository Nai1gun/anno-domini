from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from annodomini.util.fields import WYSIWYGField

class Person(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(unique=True)
    post = models.CharField(max_length=100)
    photo = ThumbnailerImageField(
        upload_to='team/photo',
        resize_source=dict(size=(212, 212), crop='smart'),
    )
    description = WYSIWYGField(blank=True)
    order = models.PositiveIntegerField(blank=True, null=True)
    
    class Meta:
        ordering = ['order', 'name']
    
    def __unicode__(self):
        return self.name
