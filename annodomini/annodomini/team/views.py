from django.views.generic.detail import DetailView
from annodomini.team.models import Person
from annodomini.util.models import get_next, get_prev


class PersonDetailView(DetailView):

    context_object_name = "person"
    model = Person

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(PersonDetailView, self).get_context_data(**kwargs)
        # Add previous and next urls
        person = context['person']
        context['next'] = "/about/team/%s/" % (get_next(Person.objects.all(), person, True).slug)
        context['prev'] = "/about/team/%s/" % (get_prev(Person.objects.all(), person, True).slug)
        
        return context