from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.views.generic.simple import redirect_to
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.base import TemplateView
from filebrowser.sites import site
from annodomini import settings
from annodomini.team.models import Person
from annodomini.news.views import NewsDetailView, NewsRedirectView
from annodomini.projects.views import ProjectDetailView, CategoryDetailView as ProjectCategoryDetailView,\
    ProjectsRedirectView
from annodomini.team.views import PersonDetailView

admin.autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(
        template_name="base.html"
    )),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/filebrowser/', include(site.urls)),
    url(r'^admin/', include(admin.site.urls)),
    (r'^grappelli/', include('grappelli.urls')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT,
    }),
    (r'^about/$', redirect_to, {'url': '/about/history/'}),
    (r'^about/team/$', ListView.as_view(
        model=Person,
        context_object_name="person_list",
    )),
    (r'^about/team/(?P<slug>[-\w]+)/$', PersonDetailView.as_view()),
    (r'^news/$', NewsRedirectView.as_view()),
    (r'^news/(?P<slug>[-\w]+)/$', 'news.views.news_list'),
    (r'^news/(?P<category__name>[-\w]+)/(?P<slug>[-\w]+)/$', NewsDetailView.as_view()),
    (r'^projects/$', ProjectsRedirectView.as_view()),
    (r'^projects/(?P<slug>[-\w]+)/$', ProjectCategoryDetailView.as_view()),
    (r'^projects/(?P<category__name>[-\w]+)/(?P<slug>[-\w]+)/$', 
        ProjectDetailView.as_view()),
    url(r'', include('django.contrib.flatpages.urls')),
)
