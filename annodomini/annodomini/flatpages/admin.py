'''
Created on Apr 8, 2012

@author: <a href="mailto:korviakov@gmail.com">Alexander Korvyakov</a>
'''
from django.db import models
from django.contrib import admin
from django.contrib.flatpages.admin import FlatpageForm, FlatPageAdmin
from django.contrib.flatpages.models import FlatPage
from annodomini.util.fields import WidgetWYSIWYG

class ExtendedFlatPageForm(FlatpageForm):
    class Meta:
        model = FlatPage


class ExtendedFlatPageAdmin(FlatPageAdmin):
    form = ExtendedFlatPageForm
    
    formfield_overrides = {
        models.TextField: {'widget': WidgetWYSIWYG},
    }
    
    fieldsets = (
        (None, {'fields': ('url', 'title', 'content', 'sites', 'template_name',)}),
    )  

admin.site.unregister(FlatPage)
admin.site.register(FlatPage, ExtendedFlatPageAdmin)