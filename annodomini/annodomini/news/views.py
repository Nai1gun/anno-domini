from django.views.generic.detail import DetailView
from annodomini.news.models import Category, News
from django.views.generic.base import RedirectView
from django.http import Http404
from annodomini.util.models import get_next, get_prev
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render_to_response
from django.template.context import RequestContext

ITEMS_ON_PAGE = 5

class NewsDetailView(DetailView):

    context_object_name = "news"
    model = News

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(NewsDetailView, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the categories
        context['category_list'] = Category.objects.all()
        # Add previous and next urls
        news = context['news']
        context['next'] = "/news/%s/%s/" % (news.category.slug,
            get_next(News.objects.filter(category=news.category), news, True).slug)
        context['prev'] = "/news/%s/%s/" % (news.category.slug,
            get_prev(News.objects.filter(category=news.category), news, True).slug)
        return context
    
class NewsRedirectView(RedirectView):
    url = "/news/"

    def get_redirect_url(self, **kwargs):
        qs = Category.objects.all()
        l = list(qs[:1])
        if l:
            first = l[0]
            redirect_url = "/news/%s/" % first.slug
            return redirect_url
        raise Http404

def news_list(request, slug):
    category = Category.objects.get(slug=slug)
    page = request.GET.get('page', 1)
    
    news_list = News.objects\
        .filter(category__slug=slug)\
        .order_by('-date')
    
    paginator = Paginator(news_list, ITEMS_ON_PAGE)
        
    try:
        news_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        news_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        news_page = paginator.page(paginator.num_pages)
        
    # Add previous and next urls    
    next = get_next(Category.objects.all(), category, False)
    if next == None:
        context_next = '/projects/'
    else:            
        context_next = '/news/%s/' % next.slug
        
    prev = get_prev(Category.objects.all(), category, False)
    if prev == None:
        context_prev = '/about/contact/'
    else:            
        context_prev = '/news/%s/' % prev.slug

    return render_to_response('news/news_list.html',
        { 'category': category,
          'page': news_page ,
          'category_list': Category.objects.all(),
          'next': context_next,
          'prev': context_prev,
        },
          context_instance=RequestContext(request))