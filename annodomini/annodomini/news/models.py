import datetime
from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField
from easy_thumbnails.files import get_thumbnailer
from annodomini.util.fields import WYSIWYGField

class Category(models.Model):
    name = models.CharField(max_length=250)       
    slug = models.SlugField(unique=True)
    
    class Meta:
        verbose_name_plural = "Categories"
    
    def __unicode__(self):
        return self.name

class News(models.Model):
    name = models.CharField(max_length=250)
    slug = models.SlugField(unique=True)
    thumbnail = ThumbnailerImageField(
        upload_to='news/thumbnail',
        resize_source=dict(size=(140, 105), crop='smart'),
    )
    date = models.DateField(default=datetime.datetime.now)
    category = models.ForeignKey(Category, related_name='news')
    preliminary_text = WYSIWYGField(blank=True, null=True)
    main_text = WYSIWYGField(blank=True, null=True)
    
    class Meta:
        verbose_name_plural = "News"
    
    def __unicode__(self):
        return self.name
    
class Photo(models.Model):
    image = models.ImageField(upload_to='news/photo')
    comment = models.CharField(max_length=250, blank=True, null=True)
    project = models.ForeignKey(News, related_name='photos', verbose_name="news")
    order = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        ordering = ['project', 'order']
   
    def normal_view(self):
        """
        This is a helper thumbnail shown in admin
        """
        thumbnailer = get_thumbnailer(self.image, relative_name=self.image.name)
        thumb = thumbnailer.get_thumbnail({'size': (100, 0)})        
            
        return thumb.tag()
       
    normal_view.allow_tags = True
    
    def __unicode__(self):
        return str(self.image.url.split("/")[-1])