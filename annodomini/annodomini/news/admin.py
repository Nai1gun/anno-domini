'''
Created on Apr 8, 2012

@author: nailgun
'''
from django.contrib import admin
from annodomini.news.models import News, Category, Photo

class PhotoInline(admin.StackedInline):
    model = Photo

class NewsAdmin(admin.ModelAdmin):
    prepopulated_fields = { 'slug': ['name'] }
    inlines = [PhotoInline]
    
class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = { 'slug': ['name'] }
    
class PhotoAdmin(admin.ModelAdmin):
    list_display = ('normal_view', 'project', 'order')
    
    def has_add_permission(self, request):
        return False

admin.site.register(News, NewsAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Category, CategoryAdmin)