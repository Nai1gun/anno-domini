from django.db import models


def get_next(qs, object, cyclic = False):
    """
    Get the next object within a queryset
    """
    next_list = qs.filter(id__gt = object.id)
    if (next_list):
        next = next_list[0]
    else:
        if (cyclic):
            next = qs[0]
        else:
            next = None
    return next

def get_prev(qs, object, cyclic = False):
    """
    Get the previous object within a queryset
    """
    prev_list = qs.filter(id__lt = object.id)
    if (prev_list):
        prev = last(prev_list)
    else:
        if (cyclic):
            prev = last(qs)
        else:
            prev = None 
    return prev

def last(qs):
    try:
        return qs.order_by('-id')[0]
    except IndexError:
            pass