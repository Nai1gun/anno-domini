'''
Created on Apr 11, 2012

@author: nailgun
'''
from django.contrib import admin
from annodomini.background.models import Background, UrlMask

class ImageInline(admin.StackedInline):
    model = UrlMask
    
class BackgroundAdmin(admin.ModelAdmin):
    inlines = [ImageInline]
    
admin.site.register(Background, BackgroundAdmin)