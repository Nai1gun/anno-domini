'''
Created on Apr 12, 2012

@author: nailgun

Add "background" context variable
'''
from annodomini.background.models import Background, UrlMask
from django.core.exceptions import ObjectDoesNotExist
import re
def background(request):
    if not re.search("^/admin/", request.path):
        path = request.path
        try:
            url_mask = UrlMask.objects.get(url_mask=path)
            background = url_mask.background
            return {
                'background': background,
            }
        except ObjectDoesNotExist:
            pass
        
    return {}