'''

Validators for forms

Created on Apr 11, 2012

@author: nailgun
'''
from django.core.exceptions import ValidationError 
   
def __validate_format(format, value):
    if not value.name.split(".")[-1].lower() == format.lower():
        raise ValidationError("This file is not in %s format." % format)
    
def validate_mp4(value):
    __validate_format('mp4', value)
    
def validate_ogg(value):
    __validate_format('ogg', value)
    
def validate_webm(value):
    __validate_format('webm', value)
    
def validate_url_mask(value):
    if not (value[0] == '/' and value [-1] == '/'):
        raise ValidationError("Url mask should start and end with '/'") 
    

