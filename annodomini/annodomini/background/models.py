from django.db import models
from django.db.models.fields.files import FileField
from annodomini.background.validators import validate_mp4, validate_ogg, validate_webm,\
    validate_url_mask
    
DEFAUL_STYLE = 'bg-default.css'
CAMERA_STYLE = 'bg-camera.css'
IPAD_STYLE = 'bg-ipad.css'
SMARTPHONE_STYLE = 'bg-smartphone.css'
CINEMA_STYLE = 'bg-cinema.css'
TV_STYLE = 'bg-tv.css'
#TODO: remove none style
NONE_STYLE = '_'
STYLE_CHOICES = (
    (DEFAUL_STYLE, 'Default'),
    (CAMERA_STYLE, 'Camera'),
    (IPAD_STYLE, 'iPad'),
    (SMARTPHONE_STYLE, 'Smartphone'),
    (CINEMA_STYLE, 'Cinema'),
    (TV_STYLE, 'TV'),
    (NONE_STYLE, 'None'),
)

class Background(models.Model):    
    name = models.CharField(max_length=250)
    preloader = models.ImageField(upload_to='background/preloader')
    video_mp4 = FileField(blank=True, null=True, upload_to="background/video", validators=[validate_mp4])
    video_ogg = FileField(blank=True, null=True, upload_to="background/video", validators=[validate_ogg])
    video_webm = FileField(blank=True, null=True, upload_to="background/video", validators=[validate_webm])
    #this field remains for backward compatibility
    url_mask = models.CharField(blank=True, null=True, editable=False, 
        max_length=250, help_text='E.g. "/" or "/about/team/"', validators=[validate_url_mask])
    style = models.CharField(max_length=20, choices=STYLE_CHOICES, default=DEFAUL_STYLE)
    
    def __unicode__(self):
        return self.name
    
class UrlMask(models.Model):
    url_mask = models.CharField(max_length=250, unique=True,\
        help_text='E.g. "/" or "/about/team/"', validators=[validate_url_mask])
    background = models.ForeignKey(Background, related_name='url masks')
    
    def __unicode__(self):
        return self.url_mask