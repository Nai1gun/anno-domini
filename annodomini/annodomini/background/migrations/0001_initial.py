# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Background'
        db.create_table('background_background', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=250)),
            ('preloader', self.gf('django.db.models.fields.files.ImageField')(max_length=100)),
            ('video_mp4', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('video_ogg', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('video_webm', self.gf('django.db.models.fields.files.FileField')(max_length=100, null=True, blank=True)),
            ('url_mask', self.gf('django.db.models.fields.CharField')(unique=True, max_length=250)),
            ('style', self.gf('django.db.models.fields.CharField')(default='bg-default.css', max_length=20)),
        ))
        db.send_create_signal('background', ['Background'])


    def backwards(self, orm):
        
        # Deleting model 'Background'
        db.delete_table('background_background')


    models = {
        'background.background': {
            'Meta': {'object_name': 'Background'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'preloader': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'style': ('django.db.models.fields.CharField', [], {'default': "'bg-default.css'", 'max_length': '20'}),
            'url_mask': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '250'}),
            'video_mp4': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'video_ogg': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'video_webm': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['background']
