# encoding: utf-8
import datetime
from south.db import db
from south.v2 import DataMigration
from django.db import models
from annodomini.background.models import Background, UrlMask

class Migration(DataMigration):

    def forwards(self, orm):
        for background in Background.objects.all():
            url_mask = UrlMask(url_mask=background.url_mask, background=background)
            url_mask.save()


    def backwards(self, orm):
        raise RuntimeError("Cannot reverse this migration.")


    models = {
        'background.background': {
            'Meta': {'object_name': 'Background'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'preloader': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'style': ('django.db.models.fields.CharField', [], {'default': "'bg-default.css'", 'max_length': '20'}),
            'url_mask': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '250'}),
            'video_mp4': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'video_ogg': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'video_webm': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'background.urlmask': {
            'Meta': {'object_name': 'UrlMask'},
            'background': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'url masks'", 'to': "orm['background.Background']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url_mask': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '250'})
        }
    }

    complete_apps = ['background']
