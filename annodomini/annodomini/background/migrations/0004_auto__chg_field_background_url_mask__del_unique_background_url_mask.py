# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Removing unique constraint on 'Background', fields ['url_mask']
        db.delete_unique('background_background', ['url_mask'])

        # Changing field 'Background.url_mask'
        db.alter_column('background_background', 'url_mask', self.gf('django.db.models.fields.CharField')(max_length=250, null=True))


    def backwards(self, orm):
        
        # User chose to not deal with backwards NULL issues for 'Background.url_mask'
        raise RuntimeError("Cannot reverse this migration. 'Background.url_mask' and its values cannot be restored.")

        # Adding unique constraint on 'Background', fields ['url_mask']
        db.create_unique('background_background', ['url_mask'])


    models = {
        'background.background': {
            'Meta': {'object_name': 'Background'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '250'}),
            'preloader': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'}),
            'style': ('django.db.models.fields.CharField', [], {'default': "'bg-default.css'", 'max_length': '20'}),
            'url_mask': ('django.db.models.fields.CharField', [], {'max_length': '250', 'null': 'True', 'blank': 'True'}),
            'video_mp4': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'video_ogg': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'video_webm': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        'background.urlmask': {
            'Meta': {'object_name': 'UrlMask'},
            'background': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'url masks'", 'to': "orm['background.Background']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'url_mask': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '250'})
        }
    }

    complete_apps = ['background']
