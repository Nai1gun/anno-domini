import os

from django.conf import settings

#Create "media/uploads" directory for django-filebrowser

upload_path = os.path.join(settings.MEDIA_ROOT, "uploads")

if upload_path and not os.path.exists(upload_path):
    os.makedirs(upload_path)